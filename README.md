## Toyota Personel Servisleri Projesi 


### Bu proje **Sakarya Üniversitesi** **Toyota Proje Yarışması** için hazırlanmıştır ve yarışma sonucunda seçilmiştir. 
Sonraki yıllarda bu yarışmaya katılacak olan arkadaşlara örnek olması açısından paylaşmak istedik.

* Proje **Maven** tabanlıdır.
* **Server**: Jetty Maven Plugin
* **Database**: Oracle
* **Front-end**: BackboneJs, RequireJs, Handlebars, Bootstrap
* **Business Layer**: Spring, Spring Security, Apache CXF
* **Persistence Layer**: JPA & Hibernate

Ayrıca proje mülakatı için hazırladığımız sunumu şu adresten inceleyebilirsiniz.
[Proje Sunumu](http://slides.com/onurozcan/toyota)