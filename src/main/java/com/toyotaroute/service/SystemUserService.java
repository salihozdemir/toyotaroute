package com.toyotaroute.service;

import com.toyotaroute.dao.SystemUserDAO;
import com.toyotaroute.dto.SystemUserDTO;
import com.toyotaroute.model.SystemUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


@Service
public class SystemUserService {

    @Autowired
    private SystemUserDAO systemUserDAO;

    @Transactional
    public SystemUserDTO save(SystemUserDTO dto) {
        SystemUser systemUser = new SystemUser();
        systemUser.convert(dto);
        try {
            systemUser = systemUserDAO.persist(systemUser);
        } catch (Exception e) {
            return null;
        }
        return dto.convert(systemUser);
    }

    @Transactional
    public SystemUserDTO edit(String id,SystemUserDTO dto) {
        SystemUser systemUser;
        try {
            systemUser = systemUserDAO.find(id);
            systemUser = systemUserDAO.merge(systemUser);
        } catch (Exception e) {
            return null;
        }
        return dto.convert(systemUser);
    }

    @Transactional
    public boolean delete(String id) {
        try {
            SystemUser systemUser = systemUserDAO.find(id);
            systemUserDAO.remove(systemUser);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public SystemUserDTO get(String id) {
        SystemUser systemUser;
        try {
            systemUser = systemUserDAO.find(id);
        } catch (Exception e) {
            return null;
        }
        return new SystemUserDTO().convert(systemUser);
    }

    public List<SystemUserDTO> getAll() {
        List<SystemUserDTO> systemUserDTOList = new ArrayList<SystemUserDTO>();
        try {
            for (SystemUser systemUser : systemUserDAO.findAll())
                systemUserDTOList.add(new SystemUserDTO().convert(systemUser));
        } catch (Exception e) {
            return null;
        }
        return systemUserDTOList;
    }

    public SystemUserDTO login(String username){
        SystemUser systemUser=systemUserDAO.login(username);
        if(systemUser==null)
            return null;
        return new SystemUserDTO().convert(systemUser);
    }
}
