package com.toyotaroute.service;

import com.toyotaroute.dao.CityDAO;
import com.toyotaroute.dto.CityDTO;
import com.toyotaroute.model.City;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.List;


@Service
public class CityService {

    @Autowired
    private CityDAO cityDAO;

    @Transactional
    public CityDTO save(CityDTO dto) {
        City city = new City();
        city.convert(dto);
        try {
            city = cityDAO.persist(city);
        } catch (Exception e) {
            return null;
        }
        return dto.convert(city);
    }

    @Transactional
    public CityDTO edit(int id, CityDTO dto) {
        City city = new City();
        try {
            city.convert(dto);
            city = cityDAO.merge(city);
        } catch (Exception e) {
            return null;
        }
        return dto.convert(city);
    }

    @Transactional
    public boolean delete(int id) {
        try {
            City city = cityDAO.find(id);
            cityDAO.remove(city);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    @Transactional
    public CityDTO get(int id) {
        City city;
        try {
            city = cityDAO.find(id);
        } catch (Exception e) {
            return null;
        }
        return new CityDTO().convert(city);
    }

    @Transactional
    public List<CityDTO> getAll() {
        List<CityDTO> cityDTOList = new ArrayList<CityDTO>();
        try {
            for (City city : cityDAO.findAll())
                cityDTOList.add(new CityDTO().convert(city));
        } catch (Exception e) {
            return null;
        }
        return cityDTOList;
    }

    @Transactional
    public List<CityDTO> searchCityByCityName(String cityName) {
        List<CityDTO> cityDTOList = new ArrayList<CityDTO>();
        try {
            for (City city : cityDAO.findAll())
                if (city.getCityName().toLowerCase().contains(cityName.toLowerCase())) {
                    cityDTOList.add(new CityDTO().convert(city));
                }
        } catch (Exception e) {
            return null;
        }
        return cityDTOList;
    }
}
