package com.toyotaroute.service;

import com.toyotaroute.dao.RouteDAO;
import com.toyotaroute.dto.CityDTO;
import com.toyotaroute.dto.RouteDTO;
import com.toyotaroute.model.City;
import com.toyotaroute.model.Route;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


@Service
public class RouteService {

    @Autowired
    private RouteDAO routeDAO;

    @Transactional
    public RouteDTO save(RouteDTO dto) {
        Route route = new Route();
        route.convert(dto);
        try {
            route = routeDAO.persist(route);
        } catch (Exception e) {
            return null;
        }
        return dto.convert(route);
    }

    @Transactional
    public RouteDTO edit(int id, RouteDTO dto) {
        Route route = new Route();
        try {
            route.convert(dto);
            route = routeDAO.merge(route);
        } catch (Exception e) {
            return null;
        }
        return dto.convert(route);
    }

    @Transactional
    public boolean delete(int id) {
        try {
            Route route = routeDAO.find(id);
            routeDAO.remove(route);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public RouteDTO get(int id) {
        Route route;
        try {
            route = routeDAO.find(id);
        } catch (Exception e) {
            return null;
        }
        return new RouteDTO().convert(route);
    }

    public List<RouteDTO> getAll() {
        List<RouteDTO> routeDTOList = new ArrayList<RouteDTO>();
        try {
            for (Route route : routeDAO.findAll())
                routeDTOList.add(new RouteDTO().convert(route));
        } catch (Exception e) {
            return null;
        }
        return routeDTOList;
    }

    public List<RouteDTO> search(String cityName, String routeTypeName) {
        List<RouteDTO> routeDTOList = new ArrayList<RouteDTO>();
        try {
            for (Route route : routeDAO.search(cityName,routeTypeName))
                    routeDTOList.add(new RouteDTO().convert(route));
        } catch (Exception e) {
            return null;
        }
        return routeDTOList;
    }
}
