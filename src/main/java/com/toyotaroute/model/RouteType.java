package com.toyotaroute.model;

import com.toyotaroute.dto.RouteDTO;
import com.toyotaroute.dto.RouteTypeDTO;
import com.toyotaroute.dto.SystemUserDTO;
import org.codehaus.jackson.annotate.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;


@Entity
@Table(name = "ROUTE_TYPE")
public class RouteType implements Serializable {

    public RouteType convert(RouteTypeDTO dto) {
        this.setId(dto.getId());
        this.setRouteTypeName(dto.getRouteTypeName());
        this.setCreateUser(new SystemUser().convert(dto.getCreateUser()));
        this.setCreateDate(dto.getCreateDate());
        this.setLastUpdateUser(new SystemUser().convert(dto.getLastUpdateUser()));
        this.setLastUpdateDate(dto.getLastUpdateDate());
        return this;
    }

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO, generator = "id_Sequence")
    @SequenceGenerator(name = "id_Sequence", sequenceName = "ID_SEQ")
    @Column(name = "ROUTE_TYPE_ID")
    private int id;

    @Column(name = "ROUTE_TYPE_NAME", nullable = false, length = 50)
    private String routeTypeName;

    @ManyToOne
    @JoinColumn(name = "CREATE_UID", nullable = false)
    private SystemUser createUser;

    @ManyToOne
    @JoinColumn(name = "LASTUPD_UID", nullable = false)
    private SystemUser lastUpdateUser;

    @Column(name = "CREATE_DATE", nullable = false)
    private Date createDate;

    @Column(name = "LASTUPD_DATE", nullable = false)
    private Date lastUpdateDate;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRouteTypeName() {
        return routeTypeName;
    }

    public void setRouteTypeName(String routeTypeName) {
        this.routeTypeName = routeTypeName;
    }

    public SystemUser getCreateUser() {
        return createUser;
    }

    public void setCreateUser(SystemUser createUser) {
        this.createUser = createUser;
    }

    public SystemUser getLastUpdateUser() {
        return lastUpdateUser;
    }

    public void setLastUpdateUser(SystemUser lastUpdateUser) {
        this.lastUpdateUser = lastUpdateUser;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getLastUpdateDate() {
        return lastUpdateDate;
    }

    public void setLastUpdateDate(Date lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RouteType routeType = (RouteType) o;
        return Objects.equals(id, routeType.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
