package com.toyotaroute.model;

import com.toyotaroute.dto.CityDTO;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;


@Entity
@Table(name = "CITY")
public class City implements Serializable {

    public City convert(CityDTO dto) {
        this.setId(dto.getId());
        this.setCityName(dto.getCityName());
        this.setCreateUser(new SystemUser().convert(dto.getCreateUser()));
        this.setCreateDate(dto.getCreateDate());
        this.setLastUpdateUser(new SystemUser().convert(dto.getLastUpdateUser()));
        this.setLastUpdateDate(dto.getLastUpdateDate());
        return this;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "id_Sequence")
    @SequenceGenerator(name = "id_Sequence", sequenceName = "ID_SEQ")
    @Column(name = "CITY_ID")
    private int id;

    @Column(nullable = false, length = 50, name = "CITY_NAME", unique = true)
    private String cityName;

    @ManyToOne()
    @JoinColumn(name = "CREATE_UID", nullable = false)
    private SystemUser createUser;

    @ManyToOne
    @JoinColumn(name = "LASTUPD_UID", nullable = false)
    private SystemUser lastUpdateUser;

    @Column(name = "CREATE_DATE", nullable = false)
    private Date createDate;

    @Column(name = "LASTUPD_DATE", nullable = false)
    private Date lastUpdateDate;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public SystemUser getCreateUser() {
        return createUser;
    }

    public void setCreateUser(SystemUser createUser) {
        this.createUser = createUser;
    }

    public SystemUser getLastUpdateUser() {
        return lastUpdateUser;
    }

    public void setLastUpdateUser(SystemUser lastUpdateUser) {
        this.lastUpdateUser = lastUpdateUser;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getLastUpdateDate() {
        return lastUpdateDate;
    }

    public void setLastUpdateDate(Date lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        City city = (City) o;
        return Objects.equals(id, city.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
