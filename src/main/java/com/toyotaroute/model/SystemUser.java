package com.toyotaroute.model;

import com.toyotaroute.dto.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


@Entity
@Table(name = "SYSADM_USER")
public class SystemUser implements Serializable {

    public SystemUser convert(SystemUserDTO dto) {
        this.setId(dto.getId());
        this.setFirstName(dto.getFirstName());
        this.setLastName(dto.getLastName());
        this.setUsername(dto.getUsername());
        this.setPassword(dto.getPassword());
        return this;
    }

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO, generator = "id_Sequence")
    @SequenceGenerator(name = "id_Sequence", sequenceName = "ID_SEQ")
    @Column(name = "SYSADM_UID")
    private int id;

    @Column(nullable = false, name = "FIRST_NAME", length = 30)
    private String firstName;

    @Column(nullable = false, name = "LAST_NAME", length = 30)
    private String lastName;

    @Column(nullable = false, name = "USERNAME", length = 30, unique = true)
    private String username;

    @Column(nullable = false, name = "PASSWORD", length = 30)
    private String password;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SystemUser that = (SystemUser) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
