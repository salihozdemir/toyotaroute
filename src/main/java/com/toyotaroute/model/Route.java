package com.toyotaroute.model;

import com.toyotaroute.dto.*;
import org.codehaus.jackson.annotate.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;


@Entity
@Table(name = "ROUTE")
public class Route implements Serializable {

    public Route convert(RouteDTO dto){

        this.setId(dto.getId());
        this.setRouteName(dto.getRouteName());
        this.setTotalDuration(dto.getTotalDuration());
        this.setPeronNo(dto.getPeronNo());
        this.setVehicleType(dto.getVehicleType());
        this.setCreateUser(new SystemUser().convert(dto.getCreateUser()));
        this.setLastUpdateUser(new SystemUser().convert(dto.getLastUpdateUser()));
        this.setLastUpdateDate(dto.getLastUpdateDate());
        this.setCreateDate(dto.getCreateDate());
        this.setCity(new City().convert(dto.getCity()));
        this.setRouteType(new RouteType().convert(dto.getRouteType()));
        return this;
    }

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO, generator = "id_Sequence")
    @SequenceGenerator(name = "id_Sequence", sequenceName = "ID_SEQ")
    @Column(name = "ROUTE_ID")
    private int id;

    @Column(name = "ROUTE_NAME", nullable = false, length = 100)
    private String routeName;

    @Column(name = "TOT_DURATION", nullable = false)
    private String totalDuration;

    @Column(name = "PERON_NO", nullable = false)
    private int peronNo;

    @Column(name = "VEHICLE_TYPE", nullable = false)
    private int vehicleType;

    @ManyToOne
    @JoinColumn(name = "CREATE_UID", nullable = false)
    private SystemUser createUser;

    @ManyToOne
    @JoinColumn(name = "LASTUPD_UID", nullable = false)
    private SystemUser lastUpdateUser;

    @ManyToOne
    @JoinColumn(name = "ROUTE_TYPE_ID", nullable = false)
    private RouteType routeType;

    @ManyToOne
    @JoinColumn(name = "CITY_ID", nullable = false)
    private City city;

    @Column(name = "CREATE_DATE", nullable = false)
    private Date createDate;

    @Column(name = "LASTUPD_DATE", nullable = false)
    private Date lastUpdateDate;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRouteName() {
        return routeName;
    }

    public void setRouteName(String routeName) {
        this.routeName = routeName;
    }

    public String getTotalDuration() {
        return totalDuration;
    }

    public void setTotalDuration(String totalDuration) {
        this.totalDuration = totalDuration;
    }

    public int getPeronNo() {
        return peronNo;
    }

    public void setPeronNo(int peronNo) {
        this.peronNo = peronNo;
    }

    public int getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(int vehicleType) {
        this.vehicleType = vehicleType;
    }

    public SystemUser getCreateUser() {
        return createUser;
    }

    public void setCreateUser(SystemUser createUser) {
        this.createUser = createUser;
    }

    public SystemUser getLastUpdateUser() {
        return lastUpdateUser;
    }

    public void setLastUpdateUser(SystemUser lastUpdateUser) {
        this.lastUpdateUser = lastUpdateUser;
    }

    public RouteType getRouteType() {
        return routeType;
    }

    public void setRouteType(RouteType routeType) {
        this.routeType = routeType;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getLastUpdateDate() {
        return lastUpdateDate;
    }

    public void setLastUpdateDate(Date lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Route route = (Route) o;
        return Objects.equals(id, route.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
