package com.toyotaroute.resource;

import com.toyotaroute.dto.CityDTO;
import com.toyotaroute.model.City;
import com.toyotaroute.security.CustomAuthenticationProvider;
import com.toyotaroute.service.CityService;
import com.toyotaroute.service.SystemUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.datetime.DateFormatter;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Component
@Path("/city")
public class CityResource {

    @Autowired
    private CityService cityService;

    @Autowired
    private SystemUserService systemUserService;

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public CityDTO save(CityDTO dto) {
        dto.setCreateUser(systemUserService.login(CustomAuthenticationProvider.getUsername()));
        dto.setLastUpdateUser(dto.getCreateUser());
        dto.setCreateDate(new Date());
        dto.setLastUpdateDate(new Date());
        return cityService.save(dto);
    }

    @PUT
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public CityDTO edit(@PathParam("id") int id, CityDTO dto) {
        dto.setLastUpdateUser(systemUserService.login(CustomAuthenticationProvider.getUsername()));
        dto.setLastUpdateDate(new Date());
        return cityService.edit(id, dto);
    }

    @DELETE
    @Path("/{id}")
    @Produces(MediaType.TEXT_PLAIN)
    public int delete(@PathParam("id") int id) {
        cityService.delete(id);
        return id;
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public CityDTO get(@PathParam("id") int id) {
        return cityService.get(id);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<CityDTO> getAll() {
        return cityService.getAll();
    }

    @GET
    @Path("/search/{cityName}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<CityDTO> search(@PathParam("cityName") String cityName) {
        if (cityName.equals("null"))
            return cityService.getAll();
        return cityService.searchCityByCityName(cityName);
    }
}
