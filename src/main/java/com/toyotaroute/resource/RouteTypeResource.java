package com.toyotaroute.resource;

import com.toyotaroute.dto.RouteTypeDTO;
import com.toyotaroute.security.CustomAuthenticationProvider;
import com.toyotaroute.service.RouteTypeService;
import com.toyotaroute.service.SystemUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.Date;
import java.util.List;


@Component
@Path("/routeType")
public class RouteTypeResource {

    @Autowired
    private RouteTypeService routeTypeService;


    @Autowired
    private SystemUserService systemUserService;

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public RouteTypeDTO save(RouteTypeDTO dto) {
        dto.setCreateUser(systemUserService.login(CustomAuthenticationProvider.getUsername()));
        dto.setLastUpdateUser(dto.getCreateUser());
        dto.setCreateDate(new Date());
        dto.setLastUpdateDate(new Date());
        return routeTypeService.save(dto);
    }

    @PUT
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public RouteTypeDTO edit(@PathParam("id") int id, RouteTypeDTO dto) {
        dto.setLastUpdateUser(systemUserService.login(CustomAuthenticationProvider.getUsername()));
        dto.setLastUpdateDate(new Date());
        return routeTypeService.edit(id, dto);
    }

    @DELETE
    @Path("/{id}")
    @Produces(MediaType.TEXT_PLAIN)
    public int delete(@PathParam("id") int id) {
        routeTypeService.delete(id);
        return id;
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public RouteTypeDTO get(@PathParam("id") int id) {
        return routeTypeService.get(id);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<RouteTypeDTO> getAll() {
        return routeTypeService.getAll();
    }

}
