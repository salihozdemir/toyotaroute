package com.toyotaroute.resource;

import com.toyotaroute.dto.StationDTO;
import com.toyotaroute.security.CustomAuthenticationProvider;
import com.toyotaroute.service.RouteService;
import com.toyotaroute.service.StationService;
import com.toyotaroute.service.SystemUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.Date;
import java.util.List;


@Component
@Path("/station")
public class StationResource {

    @Autowired
    private StationService stationService;

    @Autowired
    private SystemUserService systemUserService;

    @Autowired
    private RouteService routeService;

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public StationDTO save(StationDTO dto) {
        dto.setCreateUser(systemUserService.login(CustomAuthenticationProvider.getUsername()));
        dto.setLastUpdateUser(dto.getCreateUser());
        dto.setCreateDate(new Date());
        dto.setLastUpdateDate(new Date());
        dto.setRoute(routeService.get(dto.getRoute().getId()));
        return stationService.save(dto);
    }

    @PUT
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public StationDTO edit(@PathParam("id") int id, StationDTO dto) {
        dto.setLastUpdateUser(systemUserService.login(CustomAuthenticationProvider.getUsername()));
        dto.setLastUpdateDate(new Date());
        dto.setRoute(routeService.get(dto.getRoute().getId()));
        return stationService.edit(id, dto);
    }

    @DELETE
    @Path("/{id}")
    @Produces(MediaType.TEXT_PLAIN)
    public int delete(@PathParam("id") int id) {
        stationService.delete(id);
        return id;
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public StationDTO get(@PathParam("id") int id) {
        return stationService.get(id);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<StationDTO> getAll() {
        return stationService.getAll();
    }

    @GET
    @Path("/search/" +
            "{cityName}/" +
            "{routeTypeName}/" +
            "{routeName}/" +
            "{totalDuration}/" +
            "{peronNo}/" +
            "{vehicleType}")
    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    public List<StationDTO> search(@PathParam("cityName") String cityName,
                                   @PathParam("routeTypeName") String routeTypeName,
                                   @PathParam("routeName") String routeName,
                                   @PathParam("totalDuration") String totalDuration,
                                   @PathParam("peronNo") int peronNo,
                                   @PathParam("vehicleType") int vehicleType) {
        return stationService.search(cityName, routeTypeName, routeName, totalDuration, peronNo, vehicleType);
    }

    @GET
    @Path("/searchByAll/" +
            "{cityName}/" +
            "{routeTypeName}/" +
            "{routeName}/" +
            "{totalDuration}/" +
            "{peronNo}/" +
            "{vehicleType}/" +
            "{stationName}/" +
            "{stationNo}/" +
            "{arrivalTime}" +
            "/{departureTime}")
    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    public List<StationDTO> searchByAll(@PathParam("cityName") String cityName,
                                        @PathParam("routeTypeName") String routeTypeName,
                                        @PathParam("routeName") String routeName,
                                        @PathParam("totalDuration") String totalDuration,
                                        @PathParam("peronNo") int peronNo,
                                        @PathParam("vehicleType") int vehicleType,
                                        @PathParam("stationName") String stationName,
                                        @PathParam("stationNo") int stationNo,
                                        @PathParam("arrivalTime") String arrivalTime,
                                        @PathParam("departureTime") String departureTime) {
        if (cityName == "null")
            cityName = null;
        if (routeTypeName == "null")
            routeTypeName = null;
        if (routeName == "null")
            routeName = null;
        if (stationName == "null")
            stationName = null;
        if (arrivalTime == "null")
            arrivalTime = null;
        if (departureTime == "null")
            departureTime = null;
        return stationService.searchByAll(cityName, routeTypeName, routeName, totalDuration, peronNo,
                vehicleType, stationName, stationNo, arrivalTime, departureTime);
    }

}
