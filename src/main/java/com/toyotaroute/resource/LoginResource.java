package com.toyotaroute.resource;

import com.toyotaroute.dto.LoginDTO;
import com.toyotaroute.dto.SystemUserDTO;
import com.toyotaroute.security.CustomAuthenticationProvider;
import com.toyotaroute.service.SystemUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

@Component
@Path("/login")
public class LoginResource {

    @Autowired
    private SystemUserService systemUserService;

    @GET
    @Produces("application/json")
    public LoginDTO loginControl() {
        LoginDTO login = new LoginDTO();
        //Custom providerdan o anki oturumdan kullanıcı adı çekiliyor ve dto'nun ilgili alanına set ediliyor.
        login.setUsername(CustomAuthenticationProvider.getUsername());
        //Eğer oturum açılmamışsa null dönebileceğinden bu durum kontrol ediliyor.
        if (login.getUsername() != null) {//eğer oturum açılmışsa
            SystemUserDTO user = systemUserService.login(login.getUsername());//Username'e göre kullanıcı nesnesi dönüyor.
            //login nesnesinin ilgili alanlarına kullanıcı bilgileri set ediliyor.
            login.setFirstName(user.getFirstName());
            login.setLastName(user.getLastName());
        }
        if (login.getUsername() != null)//Kullanıcı giriş yaptıysa.
            login.setStatus(true);
        else
            login.setStatus(false);//Kullanıcı giriş yapmadıysa login durumunu false yapıyoruz.
        return login;
    }
}
