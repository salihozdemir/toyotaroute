package com.toyotaroute.dao;

import com.toyotaroute.dto.StationDTO;
import com.toyotaroute.model.Station;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.Date;
import java.util.List;

@Repository
public class StationDAO extends GenericDAO<Station> {

    public StationDAO() {
        super(Station.class);
    }

    //Anasayfadaki güzergah seçme işleminde oluşan arama hatasını düzeltmek için ayrı bir metod yazıldı.
    public List<Station> search(String cityName, String routeTypeName, String routeName, String totalDuration, int peronNo,
                                int vehicleType) {
        try {
            String query = "SELECT s FROM Station as s WHERE 1=1";

            if (cityName != null && !cityName.equals("null"))
                query += " and s.route.city.cityName =:cityName";

            if (routeTypeName != null && !routeTypeName.equals("null"))
                query += " and s.route.routeType.routeTypeName =:routeTypeName";

            if (routeName != null && !routeName.equals("null"))
                query += " and s.route.routeName =:routeName";

            if (totalDuration != null && !totalDuration.equals("null"))
                query += " and STR(s.route.totalDuration) like :totalDuration";

            if (peronNo != 0)
                query += " and s.route.peronNo =:peronNo";

            if (vehicleType != 0)
                query += " and STR(s.route.vehicleType) like :vehicleType";

            query += " order by s.id";

            Query result = entityManager.createQuery(query);

            if (!cityName.equals("null"))
                result.setParameter("cityName", cityName);

            if (!routeTypeName.equals("null"))
                result.setParameter("routeTypeName", routeTypeName);

            if (!routeName.equals("null"))
                result.setParameter("routeName", routeName);

            if (totalDuration != null && !totalDuration.equals("null"))
                result.setParameter("totalDuration", "%" + totalDuration + "%");

            if (peronNo != 0)
                result.setParameter("peronNo", peronNo);

            if (vehicleType != 0)
                result.setParameter("vehicleType", "%" + vehicleType + "%");

            return result.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    //Tüm parametrelere göre arama metodu.
    public List<Station> searchByAll(String cityName, String routeTypeName, String routeName, String totalDuration, int peronNo,
                                     int vehicleType, String stationName, int stationNo, String arrivalTime, String departureTime) {
        try {
            String query = "SELECT s FROM Station as s WHERE 1=1";

            if (cityName != null && !cityName.equals("null"))
                query += " and LOWER(s.route.city.cityName) like :cityName";

            if (routeTypeName != null && !routeTypeName.equals("null"))
                query += " and LOWER(s.route.routeType.routeTypeName) like :routeTypeName";

            if (routeName != null && !routeName.equals("null"))
                query += " and LOWER(s.route.routeName) like :routeName";

            if (totalDuration != null && !totalDuration.equals("null"))
                query += " and STR(s.route.totalDuration) like :totalDuration";

            if (peronNo != 0)
                query += " and s.route.peronNo =:peronNo";

            if (vehicleType != 0)
                query += " and STR(s.route.vehicleType) like :vehicleType";

            if (stationName != null && !stationName.equals("null"))
                query += " and LOWER(s.stationName) like :stationName";

            if (stationNo != 0)
                query += " and STR(s.stationNo) like :stationNo";

            if (arrivalTime != null && !arrivalTime.equals("null"))
                query += " and LOWER(s.arrivalTime) like :arrivalTime";

            if (departureTime != null && !departureTime.equals("null"))
                query += " and LOWER(s.departureTime) like :departureTime";

            query += " order by s.id";//Sıralamanın her seferinde değişmemesi için.

            Query result = entityManager.createQuery(query);

            //##### Gelen değer null ya da 0 ise arama sorgusuna dahil edilmez ####
            if (!cityName.equals("null"))
                result.setParameter("cityName", "%" + cityName.toLowerCase() + "%");

            if (!routeTypeName.equals("null"))
                result.setParameter("routeTypeName", "%" + routeTypeName.toLowerCase() + "%");

            if (!routeName.equals("null"))
                result.setParameter("routeName", "%" + routeName.toLowerCase() + "%");

            if (!stationName.equals("null"))
                result.setParameter("stationName", "%" + stationName.toLowerCase() + "%");

            if (!arrivalTime.equals("null"))
                result.setParameter("arrivalTime", "%" + arrivalTime.toLowerCase() + "%");

            if (!departureTime.equals("null"))
                result.setParameter("departureTime", "%" + departureTime.toLowerCase() + "%");

            if (totalDuration != null && !totalDuration.equals("null"))
                result.setParameter("totalDuration", "%" + totalDuration + "%");

            if (peronNo != 0)
                result.setParameter("peronNo", peronNo);

            if (vehicleType != 0)
                result.setParameter("vehicleType", "%" + vehicleType + "%");

            if (stationNo != 0)
                result.setParameter("stationNo", "%" + stationNo + "%");

            return result.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
