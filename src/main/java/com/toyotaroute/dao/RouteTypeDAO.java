package com.toyotaroute.dao;

import com.toyotaroute.model.RouteType;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


@Repository
public class RouteTypeDAO extends GenericDAO<RouteType> {
    public RouteTypeDAO(){
        super(RouteType.class);
    }
}
