package com.toyotaroute.dao;

import com.toyotaroute.model.Route;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;


@Repository
public class RouteDAO extends GenericDAO<Route> {
    public RouteDAO() {
        super(Route.class);
    }

    //Şehir adı ve Güzergah tipi adı parametrelerine göre arama metodu.
    public List<Route> search(String cityName, String routeTypeName) {
        try {
            String query = "SELECT s FROM Route as s where 1=1";

            if (cityName != null && !cityName.equals("null"))
                query += " and s.city.cityName =:cityName";

            if (routeTypeName != null && !routeTypeName.equals("null"))
                query += " and s.routeType.routeTypeName =:routeTypeName";

            query += " order by s.id"; //Sıralamanın bozulmaması için..

            Query result = entityManager.createQuery(query);

            if (cityName != null && !cityName.equals("null"))
                result.setParameter("cityName", cityName);

            if (routeTypeName != null && !routeTypeName.equals("null"))
                result.setParameter("routeTypeName", routeTypeName);

            return result.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
