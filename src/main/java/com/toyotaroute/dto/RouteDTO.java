package com.toyotaroute.dto;

import com.toyotaroute.model.City;
import com.toyotaroute.model.Route;
import com.toyotaroute.model.Station;
import org.codehaus.jackson.annotate.JsonIgnore;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;


public class RouteDTO implements Serializable {

    public RouteDTO convert(Route entity) {
        this.setId(entity.getId());
        this.setRouteName(entity.getRouteName());
        this.setTotalDuration(entity.getTotalDuration());
        this.setPeronNo(entity.getPeronNo());
        this.setVehicleType(entity.getVehicleType());
        this.setCreateUser(new SystemUserDTO().convert(entity.getCreateUser()));
        this.setLastUpdateUser(new SystemUserDTO().convert(entity.getLastUpdateUser()));
        this.setLastUpdateDate(entity.getLastUpdateDate());
        this.setCreateDate(entity.getCreateDate());
        this.setCity(new CityDTO().convert(entity.getCity()));
        this.setRouteType(new RouteTypeDTO().convert(entity.getRouteType()));
        return this;
    }

    private int id;

    private String routeName;

    private String totalDuration;

    private int peronNo;

    private int vehicleType;

    private SystemUserDTO createUser;

    private SystemUserDTO lastUpdateUser;

    private RouteTypeDTO routeType;

    private CityDTO cityDTO;

    private Date createDate;

    private Date lastUpdateDate;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRouteName() {
        return routeName;
    }

    public void setRouteName(String routeName) {
        this.routeName = routeName;
    }

    public String getTotalDuration() {
        return totalDuration;
    }

    public void setTotalDuration(String totalDuration) {
        this.totalDuration = totalDuration;
    }

    public int getPeronNo() {
        return peronNo;
    }

    public void setPeronNo(int peronNo) {
        this.peronNo = peronNo;
    }

    public int getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(int vehicleType) {
        this.vehicleType = vehicleType;
    }

    public SystemUserDTO getCreateUser() {
        return createUser;
    }

    public void setCreateUser(SystemUserDTO createUser) {
        this.createUser = createUser;
    }

    public SystemUserDTO getLastUpdateUser() {
        return lastUpdateUser;
    }

    public void setLastUpdateUser(SystemUserDTO lastUpdateUser) {
        this.lastUpdateUser = lastUpdateUser;
    }

    public RouteTypeDTO getRouteType() {
        return routeType;
    }

    public void setRouteType(RouteTypeDTO routeType) {
        this.routeType = routeType;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getLastUpdateDate() {
        return lastUpdateDate;
    }

    public void setLastUpdateDate(Date lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    public CityDTO getCity() {
        return cityDTO;
    }

    public void setCity(CityDTO cityDTO) {
        this.cityDTO = cityDTO;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RouteDTO route = (RouteDTO) o;
        return Objects.equals(id, route.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
