define(['jquery', 'underscore', 'backbone'], function ($, _, Backbone) {
    var RouteTypes = Backbone.Collection.extend({
        url: "/toyota-route/rest/routeType"
    });
    return RouteTypes;
});


