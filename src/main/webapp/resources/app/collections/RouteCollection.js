define(['jquery', 'underscore', 'backbone'], function ($, _, Backbone) {
    var Routes = Backbone.Collection.extend({
        url: "/toyota-route/rest/routes"
    });
    return Routes;
});


