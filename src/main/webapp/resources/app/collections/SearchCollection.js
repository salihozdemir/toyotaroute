define(['jquery', 'underscore', 'backbone', 'app/models/StationModel'], function ($, _, Backbone, Station) {
    var Search = Backbone.Collection.extend({
        model: Station,
        initialize: function (models, options) {
            this.cityName = options.cityName;
            this.routeTypeName = options.routeTypeName;
            this.routeName = options.routeName;
            this.totalDuration = options.totalDuration;
            this.peronNo = options.peronNo;
            this.vehicleType = options.vehicleType;
            this.stationName = options.stationName;
            this.stationNo = options.stationNo;
            this.departureTime = options.departureTime;
            this.arrivalTime = options.arrivalTime;
        },
        //Uygun olmayan her parametreye varsayılan değer atanıyor.
        url: function () {
            if (typeof this.cityName =='undefined' || this.cityName == "" || this.cityName=="Şehir Seçiniz" || !isNaN(this.cityName))
                this.cityName = "null";
            if (typeof this.routeTypeName =='undefined' || this.routeTypeName == "" || this.routeTypeName=="Güzergah Tipi Seçiniz")
                this.routeTypeName = "null";
            if (typeof this.routeName =='undefined' || this.routeName == "" || this.routeName=="Güzergah Seçiniz")
                this.routeName = "null";
            if (typeof this.totalDuration =='undefined' || this.totalDuration == "")
                this.totalDuration = "null";
            if (typeof this.peronNo =='undefined' || this.peronNo == "" || isNaN(this.peronNo))
                this.peronNo = 0;
            if (typeof this.vehicleType =='undefined' || this.vehicleType == "" || isNaN(this.vehicleType))
                this.vehicleType = 0;
            if (typeof this.stationName =='undefined' || this.stationName == "" || this.stationName=="Durak Seçiniz")
                this.stationName = "null";
            if (typeof this.stationNo =='undefined' || this.stationNo == "" || isNaN(this.stationNo))
                this.stationNo = 0;
            if (typeof this.departureTime =='undefined' || this.departureTime == "")
                this.departureTime = "null";
            if (typeof this.arrivalTime =='undefined' || this.arrivalTime == "")
                this.arrivalTime = "null";
            return "rest/station/searchByAll/"
                + this.cityName + "/" + this.routeTypeName + "/" + this.routeName + "/"
                + this.totalDuration + "/" + this.peronNo + "/" + this.vehicleType + "/" + this.stationName + "/"
                + this.stationNo + "/" + this.departureTime + "/" + this.arrivalTime;
        }
    });
    return Search;
});




