define(['jquery', 'underscore', 'backbone'], function ($, _, Backbone) {
    var Cities = Backbone.Collection.extend({
        url: "/toyota-route/rest/city"
    });
    return Cities;
});


