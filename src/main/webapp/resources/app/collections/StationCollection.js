define(['jquery', 'underscore', 'backbone'], function ($, _, Backbone) {
    var Stations = Backbone.Collection.extend({
        url: "/toyota-route/rest/station"
    });
    return Stations;
});




