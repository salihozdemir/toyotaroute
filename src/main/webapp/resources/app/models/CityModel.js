define(['jquery', 'underscore', 'backbone'], function ($, _, Backbone) {
    var City = Backbone.Model.extend({
        urlRoot: "/toyota-route/rest/city",
        validate: function (attrs) {
            if (attrs.cityName == "") {
                return "Şehir Adı Boş Geçilemez!";
            }
        }
    });
    return City;
});


