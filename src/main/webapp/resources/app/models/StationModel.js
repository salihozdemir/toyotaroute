define(['jquery', 'underscore', 'backbone'], function ($, _, Backbone) {
    var Station = Backbone.Model.extend({
        urlRoot: "/toyota-route/rest/station",
        validate: function (attrs, options) {
            if (attrs.route.id == 0) {
                return "routeSelectStationPage";
            }
            if (attrs.stationName == "") {
                return "stationNameAdd";
            }
            if (attrs.stationNo == 0) {
                return "stationNoAdd";
            }
            if (attrs.arrivalTime == 0) {
                return "arrivalTimeAdd";
            }
            if (attrs.departureTime == 0) {
                return "departureTimeAdd";
            }
        }
    });
    return Station;
});




