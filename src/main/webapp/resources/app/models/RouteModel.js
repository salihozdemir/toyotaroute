define(['jquery', 'underscore', 'backbone'], function ($, _, Backbone) {
    var Route = Backbone.Model.extend({
        urlRoot: "/toyota-route/rest/routes",
        validate: function (attrs, options) {
            if (attrs.city.id == 0) {
                return "citySelectAdd";
            }
            if (attrs.routeType.id == 0) {
                return "routeTypeSelectAdd";
            }
            if (attrs.routeName == "") {
                return "routeNameAdd";
            }
            if (attrs.totalDuration == "") {
                return "totalDurationAdd";
            }
            if (attrs.peronNo == 0) {
                return "peronNoAdd";
            }
            if (attrs.vehicleType == 0) {
                return "vehicleTypeAdd";
            }
        }
    });
    return Route;
});

