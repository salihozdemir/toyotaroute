define(['jquery', 'underscore', 'backbone', 'handlebars',
    'app/collections/RouteCollection',
    'app/models/RouteModel',
    'text!app/templates/select-route-template.html'], function ($, _, Backbone, Handlebars, Routes, Route,SelectRouteTemplate) {

    var routes = new Routes();
    var SelectRouteView = Backbone.View.extend({
        model: Route,
        render: function (routes) {
            var template = Handlebars.compile(SelectRouteTemplate);
            var myHtml = template({routes: routes.toJSON()});
            $("#routeSelectAdd").html(myHtml);
            $("#routeSelectStationPage").html(myHtml);
            $(".routeSelect").html(myHtml);
        }
    });

    return {
        routes:routes,
        SelectRouteView:SelectRouteView
    };
});




