define(['jquery', 'underscore', 'backbone',
    'handlebars',
    'app/collections/RouteTypeCollection',
    'app/models/RouteTypeModel',
    'text!app/templates/select-routeType-template.html'], function ($, _, Backbone, Handlebars, RouteTypes,
                                                                          RouteType, SelectRouteTypeTemplate) {
    var routeTypes = new RouteTypes();
    var SelectRouteTypeView = Backbone.View.extend({
        model: RouteType,
        render: function (routeTypes) {
            var template = Handlebars.compile(SelectRouteTypeTemplate);
            var myHtml = template({routeTypes: routeTypes.toJSON()});
            $("#routeTypeSelectAddHome").html(myHtml);
            $("#routeTypeSelectAdd").html(myHtml);
            $(".routeTypeSelect").html(myHtml);
        }
    });

    return {
        SelectRouteTypeView:SelectRouteTypeView,
        routeTypes:routeTypes
    };
});


