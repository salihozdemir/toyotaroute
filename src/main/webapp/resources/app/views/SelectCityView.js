define(['jquery', 'underscore', 'backbone', 'handlebars',
    'app/collections/CityCollection',
    'app/models/CityModel',
    'text!app/templates/select-city-template.html'], function ($, _, Backbone, Handlebars, Cities, City,SelectCityTemplate) {
    var cities = new Cities();
    var SelectCityView = Backbone.View.extend({
        model: City,
        render: function (cities) {
            var template = Handlebars.compile(SelectCityTemplate);
            var myHtml = template({cities: cities.toJSON()});
            $("#citySelectAdd").html(myHtml);
            $("#citySelectAddHome").html(myHtml);
            $(".citySelect").html(myHtml);
        }
    });

    return {
        SelectCityView:SelectCityView,
        cities:cities
    };
});




